const https = require('https');
require('dotenv').config();

function isMuddy(req) {
  const api_key = process.env.API_KEY;

  let zip = req.query.zip ? req.query.zip : 48104;
  const uri = 'https://api.openweathermap.org/data/2.5/forecast?zip=' + zip + ',us&appid=' + api_key;
  
  return new Promise(function(resolve, reject) {
    https.get(uri, (resp) => {
      let data = '';

      resp.on('data', (chunk) => {
        data += chunk;
      });

      resp.on('end', () => {
        try {
          let jsonized = JSON.parse(data);
          let rains_while_above_freezing = false;
          let now = new Date();

          for (let item of jsonized.list) {
            let item_date = new Date(item.dt * 1000);
            let diff_time = item_date - now;
            let diff_days = diff_time / (1000 * 60 * 60 * 24);
            
            if (diff_days < 3 &&
                item.rain !== undefined && 
                item.main.temp_max > 273.15) {
              rains_while_above_freezing = true;
            }
          }

          resolve(rains_while_above_freezing);
        } catch (e) {
          reject(e);
        }
      });
    }).on("error", (err) => {
      console.err(err.message);
      reject(err);
    });
  });
}

module.exports = isMuddy;

