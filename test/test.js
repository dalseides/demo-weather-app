const nock = require('nock');
const request = require('supertest')("https://api.openweathermap.org");
const request2 = require('supertest')("http://api.postcodes.io");
const expect = require('chai').expect;
const assert = require('assert');
const isMuddy = require('../controllers/index.js');

describe("Testing controller with mock data", function() {
  it("returns true when muddy", function (done) {
    let res = {
      "list": [{
        "dt": (Date.now() / 1000) + (60 * 60 * 3),
        "rain": {
          "3h": 0.87
        },
        "main": {
          "temp_max": 9001
        }
      }]
    }

    nock("https://api.openweathermap.org")
      .get(/data.*/)
      .reply(200, JSON.stringify(res));

    let req = { query: { zip: 1 } };

    isMuddy(req).then(function(is_muddy) {
      expect(is_muddy).to.equal(true);
      done();
    });
  });

  it("returns false when not muddy", function (done) {
    let res = {
      "list": [{
        "dt": (Date.now() / 1000) + (60 * 60 * 3),
        "main": {
          "temp_max": 9001
        }
      }]
    }

    nock("https://api.openweathermap.org")
      .get(/data.*/)
      .reply(200, JSON.stringify(res));

    let req = { query: { zip: 1 } };

    isMuddy(req).then(function(is_muddy) {
      expect(is_muddy).to.equal(false);
      done();
    });
  });

  it("returns false when rains too late", function (done) {
    let res = {
      "list": [{
        "dt": (Date.now() / 1000) + (4 * 24 * 60 * 60),
        "main": {
          "temp_max": 9001
        }
      }]
    }

    nock("https://api.openweathermap.org")
      .get(/data.*/)
      .reply(200, JSON.stringify(res));

    let req = { query: { zip: 1 } };

    isMuddy(req).then(function(is_muddy) {
      expect(is_muddy).to.equal(false);
      done();
    });
  });

  it("throws error when API is down or bad", function (done) {
    nock("https://api.openweathermap.org")
      .get(/data.*/)
      .reply(500);

    let req = { query: { zip: 1 } };

    isMuddy(req).then(function () {
        throw new Error('was not supposed to succeed');
      }).catch(function(e) {
        expect(e).to.not.be.null;
        done();
      });
  });
});

