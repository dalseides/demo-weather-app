# demo-weather-app

Demo app that reads from a weather API and displays some data as part of a company's interview process.

To run a dockerized image of this app:
1. Install Docker
2. From a command prompt, run `docker run -p 3000:3000 -e API_KEY=<your API key here> -d dalseides/demo-weather-app`
3. Navigate to localhost:3000 (for Ann Arbor) or localhost:3000/?zip=<US zip code> to see weather for an arbitrary location within the US.

To run locally:
1. Install NodeJS
2. From a command prompt, navigate to the the directory housing the project and run `npm i`
2.a If on Windows, add your API_KEY to a file named `.env` at the project's root level
3. Run `API_KEY=<your API key here> npm start`
3.a If on Windows, just run `npm start`
4. Navigate to localhost:3000 (for Ann Arbor) or localhost:3000/?zip=<US zip code> to see weather for an arbitrary location within the US.

To run unit tests:
1. Install NodeJS
2. From a command prompt, navigate to the the directory housing the project and run `npm i`
3. Run `npm test`

