const express = require('express');
const router = express.Router();
const isMuddy = require('../controllers/index.js');

// TODO: add unit tests
// TODO: dockerize
function indexQuery(req, res, next) {
  isMuddy(req).then(function(is_muddy) {
    res.render('index', { title: 'Demo Weather App', weather: is_muddy ? ' muddy' : ' not muddy' });
  }).catch(function(err) {
    res.render('index', { title: 'Demo Weather App', weather: ' mysterious and difficult to predict.'});
  });
}

/* GET home page. */
router.get('/', indexQuery);

/* GET bypassing issues with my nginx rewrite config */
router.get('/node', indexQuery);

module.exports = router;
